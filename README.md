## GitLab Learn Security Training Course

Repository for the GitLab Learn Security Training Course materials

Included files:

[GitLab Learn Security Pt.1 raw file](GitLabLearnSecurity-CourseCreationPart1.MP4) (688 MB)
[GitLab Learn Security Pt.2 raw file](GitLabLearnSecurity-CourseCreationPart2.MP4) (1.6 GB)
[GitLab Training Pt.1 kdenlive file](gitlab-training-1.kdenlive) (69 KB)
[GitLab Training Pt.2 kdenlive file](gitlab-training-2.kdenlive) (80 KB)
[GitLab Training Pt.1 Final Edit](gitlab-training-1.mp4) (456 MB)
[GitLab Training Pt.2 Final Edit](gitlab-training-2.mp4) (1.1 GB)
